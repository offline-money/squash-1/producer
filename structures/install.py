

'''
	python3 install.py
'''


target = "modules_required_pip"

modules = " ".join ([
	'"click"',
	'"flask"',
	'"onesie"'
])

import os
os.system (f"rm -rf { target }")
os.system (f"pip install { modules } --upgrade --target { target }")

